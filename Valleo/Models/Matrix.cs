﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Valleo.Models
{
    public class Matrix
    {
        public int ID { get; set; }
        [Range(3, 5)]
        public int orderMatrix { get; set; }
 
    }

}
