﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Valleo.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Valleo.Data;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Claims;

namespace Valleo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
     
        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var users = _context.Users.ToList();
            List<string> s = new List<string>();

            foreach (var item in users)
            {
                s.Add(item.UserName.ToString() + " " + item.LockoutEnabled.ToString() + " " + item.LockoutEnd.ToString());
            }
           
            return View(s);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
           
          //  _logger.LogInformation("URL приложения: {ID}", userId);
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            
            return View();
        }


      

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
